<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Projekt Weltrekord - Registrieren</title>

    <script src="js/utility.js" defer></script>
    <script src="js/check-register.js" async></script>

    <?php include 'php/layout.php' ?>
</head>

<body>
    <!-- Navigation Bar -->
    <?php getNav("index") ?>

    <div class="container">
        <?php include_once("php/check-login.php"); ?>

        <h1>Teilnehmer Registrieren</h1>
        <div class="row">
            <form class="col s12" onsubmit="return checkRegister()" action="php/add-person.php" method="POST">
                <div class="row">
                    <div class="input-field col s6">
                        <input id="first_name" name="first_name" type="text" class="validate">
                        <label for="first_name">Vorname</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" name="last_name" type="text" class="validate">
                        <label for="last_name">Nachname</label>
                    </div>
                </div>
                <div class="row">
                    <button class="btn waves-effect waves-light" type="submit" name="action" style="float:right">Teilnehmer hinzufügen
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </form>
        </div>
        <div style="height:15px"></div>
        <div class="row">
            <table>
                <tr>
                    <th>Nummer</th>
                    <th>Vorname</th>
                    <th>Nachname</th>
                </tr>

                <?php
                    include "php/_sql-login.php";

                    $sql = "SELECT ID, first_name, last_name FROM personen";
                    $result = $conn->query($sql);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while($row = $result->fetch_assoc()) {
                            echo "<tr>";
                            echo "<td>" . strip_tags($row["ID"]) . "</td>";
                            echo "<td>" . strip_tags($row["first_name"]) . "</td>";
                            echo "<td>" . strip_tags($row["last_name"]) . "</td>";
                            echo "</td>";
                        }
                    } else {
                        echo "<tr>";
                        echo "<td columnspan='3'>Keine Einträge vorhanden</td>";
                        echo "</td>";
                    }

                    $conn->close();
                ?> 
            </table>
        </div>
    </div>
   
    <!-- Logout Modal -->
    <?php getModal() ?>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var elems = document.querySelectorAll('.modal');
            var instances = M.Modal.init(elems);
        });
    </script>
</body>
</html>
        