<?php
    // get config file, no matter where this file was called from
    if (file_exists('config.php')){
        include_once 'config.php';
    } else {
        include_once '../config.php';
    }
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
?>