<?php
    include "_sql-login.php";

    // define SQL statement
    $sql= "SELECT personen1.first_name AS first1, personen1.last_name AS last1, personen2.first_name AS first2, personen2.last_name AS last2 From prosts LEFT JOIN personen AS personen1 ON id1=personen1.ID LEFT JOIN personen AS personen2 ON id2=personen2.ID ORDER BY prosts.dateOfCreation DESC LIMIT 7;";

    // execute SQL statement
    $result = $conn->query($sql);

    // run through every result
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
          // output the result as a string that's formated like a CSV but seperated with a Paragraph-Sign (§)
          echo $row["first1"]. " " . $row["last1"] ."§";
          echo $row["first2"]. " " . $row["last2"] ."§";
        }
      } else
        // output "Error-Message" if no data has been entered yet
        echo "No Data yet";

    $conn->close();
?>