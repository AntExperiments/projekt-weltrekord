<?php
    include "_sql-login.php";

    // get variables
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];

    // prepare SQL statement
    $stmt = $conn->prepare("INSERT INTO personen (first_name, last_name) VALUES (?, ?)");

    // define datatypes as string
    $stmt->bind_param('ss', $first_name, $last_name);

    if ($stmt->execute() === TRUE)
        // display Success-Message
        header('Location: ..?success=true');
    else
        // display Error-Message
        header('Location: ..?error=true');

    $conn->close();
?> 