# Projekt Weltrekord

# 1.1 Aufgabenstellung und Organisation (Informieren)

## 1.1.1 Aufgabenstellung

Eine Web-App erstellen, die dazu dient, einen Weltrekord zu dokumentieren.

Sie soll ein Formular besitzen, um die Teilnehmer mit Vor- und Nachnamen zu erfassen. Dann soll es ein weiteres Formular geben, dass die Prosts erfasst und dann eine weitere Seite die live-statistiken dazu anzeigt.

Das Projekt sollte ebenfalls vor SQL-Injektions und Cross-Site-Scripting geschützt sein.

## 1.1.4 Dokumentation

Wird in Word erstellt und als PDF abgeben.

Abgabe per Email als ZIP: Produkt, Dokumentation und Export der Datenbank.

# 1.2 Werkzeuge / Verwendete Tools (Entscheiden)

UI-Framework: MaterializeCSS ([https://materializecss.com/](https://materializecss.com/)) + ChartJS(https://www.chartjs.org/)
 Server: Apache + mySQL + PHP (XAMPP/LAMPP)
 Programmiersprachen: JavaScript, PHP (+ HTML/CSS)
 Versionierungswerkzeug: git

Verwendeter Editor: Visual Studio Code (genauer VSCodium: github.com/VSCodium/vscodium)

Zur Verfügung gestellt wurde: Auftrag

# 1.4 UI-Mockup

1.5 Arbeitsjournal (Realisieren)

## 1.5.1 Tag 1

Datum: Dienstag, 12.05.2020

**Tagesziele:**

- Analyse des Auftrags
- Zeitplan erstellen
- GUI-Mockup erstellen
- Datenbank erstellen
- Tests erstellen
- Projekt beginnen
- Formulare erstellen

**Reflexion:**

Ich hatte heute sehr viele verschiedene Aufgaben zu erledigen, aber es lief ziemlich gut.

## 1.5.2 Tag 2

Datum: Montag, 18.05.2020

**Tagesziele:**

- Mit Statistikseite fortfahren
- Implementierung von Sicherungen gegen SQL-Injektions
- Code-Optimierungen (Letztes mal blieb nicht genug Zeit um den Code zu optimieren)
- Installer

**Reflexion:**

Der Tag lief ziemlich gut, ich konnte sehr effektiv arbeiten. Da ich im HomeOffice war wurde ich auch nicht abgelenkt. Ich bin mit der Software soweit, bis jetzt, zufrieden.

## 1.5.2 Tag 3

Datum: Dienstag, 19.05.2020

**Tagesziele:**

- Dokumentation fertigstellen
- Tests durchführen
- Abgabe

**Reflexion:**

Heute konnte ich einige letzte Verbesserungen fertigstellen. Zudem konnte ich viel Zeit in die Dokumentation investieren. Ich bin zufrieden, wie der Tag verlaufen ist.

# 1.6 Tests (Kontrollieren)

| Nr | Testname | Absicht | Vorgehen | Erwartetes Ergebnis | Tatsächliches Ergebnis | OK? |
| --- | --- | --- | --- | --- | --- | --- |
| 1 | Installtions-Script funnktioniert | Software installieren auf einer lehren Datenbank | Install.php öffnen, korrekte login-daten eingeben, auf statistik-seite schauen ob Daten vorhanden | Daten vorhanden | Daten vorhanden | OK |
| 2 | Person hinzufügen | Person hinzufügen | Auf index.php gehen, Vorname: Max, Nachname: Muster eingeben und absenden. Danach ganz herunterscrollen und sehen, ob die Person nun in der Liste ist | Name Sichtbar | Name Sichtbar | OK |
| 3 | Prosts hinzufügen | Prosts hinzufügen | Auf die «Prosts Registrieren» Seite wechseln, eine Person mit dem Drop-Down auswählen und einige Nummern (e.g. 1, 5, 10, 7) eingeben. Danach auf die Statistiken-Seite gehen und überprüfen, ob die Prosts eingetragen wurden | Prosts eingetragen | Prosts eingetragen | OK |
| 4 | Falsche Prosts-ID | Überprüfen, ob das System erkennt, dass die ID nicht existiert | Auf die «Prosts Registrieren» Seite wechseln, eine Person mit dem Drop-Down auswählen und als Nummer 99999 eingeben | Eine Error-Meldung erscheint | Eine Error-Meldung erscheint | OK |
| 5 | \&lt;Script\&gt; als Name | Überprüfen, ob ein Script-Tag eingegeben werden kann. | Auf die «Teilnehmer Registrieren» Seite gehen, als Vor-/und Nachnamen «\&lt;script\&gt;alert(&#39;test&#39;)\&lt;/script\&gt;» eingeben | Kein Popup erscheint, Skript wird nicht ausgeführt | Kein Popup erscheint, Skript wird nicht ausgeführt | OK |
| 6 | SQL-injection als Name | Überprüfen ob eine SQL-injection verhindert wird | Auf die «Teilnehmer Registrieren» Seite gehen, als Vor-/und Nachnamen «&#39;&#39;;DELETE \* FROM personen;&#39;&#39;» eingeben | SQL wird nicht ausgeführt, Name wird normal eingetragen | SQL wird nicht ausgeführt, Name wird normal eingetragen | OK |
| 7 | Login-Funktion | Überprüfen, ob das Passwort überprüft wird | Sich ausloggen und mit irgendeiner falschen Email-/und Password-Kombo einzuloggen (e.g. [foo@bar.com](mailto:foo@bar.com), foobar) | Einloggen nicht erfolgreich | Einloggen nicht erfolgreich | OK |
| 8 | Mobil-Optimierung | Prüfen, ob die Web-App auch auf einem Smartphone gut aussieht | Fenster des Browsers auf Smartphone Grösse verkleinern | Navbar passt sich an, Content wird untereinander angezeigt | Navbar passt sich an, Content wird untereinander angezeigt | OK |

# 1.7 Reflexion (Auswerten)

## 1.7.1 Das lief gut

Meine Ziele habe ich erreicht. Ich bin mit dem Endergebnis Zufrieden.

Mit dem GUI bin ich zufrieden, jedoch hätte ich (falls ich mehr Zeit gehabt hätte) noch die Statistik-Seite erweitert und modular gemacht (so, dass der Nutzer auswählen kann, was er angezeigt haben möchte). Die Diagramme jedoch könnten von ein paar Optimierungen noch profitieren.

Das Backend ist okay, nicht super aber es erfüllt seinen Zweck. Die Orderstruktur könnte einige Aufräumarbeiten vertragen, jedoch funktioniert es gut und widersteht auch SQL-Injections und XSS versuchen.

## 1.7.2 Das nehme ich fürs nächste Mal mi

Nächstes Mahl sollte ich versuchen das Backend «modularer» zu erstellen, dies würde vieles vereinfachen. Ich sollte mir angewöhnen mehr Objektorientiert zu programmieren. Ich habe zwar einige Teile dieses Programms Objektorientiert geschrieben, jedoch nicht überall.

Zudem nehme ich mit, dass ich nun weiss, wie man Diagramme mit chart.js erstellt. Auch habe ich nun viel Übung mit PHP und SQL, welches mir vor diesem Projekt noch einige Probleme bereitet hat.

## 1.7.3 Rückblick

Alles in allem bin ich mit dem Ergebnis zufrieden. Es funktioniert einwandfrei und ist recht robust.

Bei diesem Projekt konnte ich einiges lernen und auch vieles anwenden. Mir hat die Arbeit, an diesem Projekt, Spass gemacht, etwas mehr Zeit hätte in meinen Augen aber nicht geschadet. Ich hätte noch viele Ideen, wie ich diese Applikation noch verbessen könnte.

# 2 Installations- und Benutzeranleitung

## 2.1 Vorbereitung und Installation

Erstellen Sie eine SQL-Datenbank, mit einem Namen Ihrer Wahl, zudem erstellen Sie einen Nutzer mit INSERT-/und SELECT-Berechtigung.

Gehen sie auf _ **install.php** _ und geben Sie die korrekten Daten der Datenbank und des Datenbank-Nutzers ein. Danach geben sie das gewünscht Login-Passwort und Nutzernamen ein und wählen an, ob Sie Beispiel-Daten haben möchten, oder nicht.

## 2.2 Nutzung

# 2.2.1 «Teilnehmer Registrieren»-Seite

Auf dieser Seite können sie neue Teilnehmer registrieren, einfach den Vor-/und Nachnamen eingeben und auf «Teilnehmer hinzufügen» drücken.

Ebenfalls können Sie hier alle Teilnehmer in einer Auflistung unten sehen.

# 2.2.2 «Prosts Registrieren»-Seite

Der Zweck dieser Seite ist es, dass Sie die Prosts einfach erfassen können.

Wählen sie jeweils die Person, für die Sie den Prost erfassen möchten, und geben Sie danach die Nummern der anderen Personen ein. Es müssen nicht alle Felder ausgefüllt werden.

# 2.2.1 «Statistiken»-Seite

Diese Seite ist ein einfacher und zentraler Ort, um alle verschiedenen Statistiken einzusehen. Die Graphen zeigen Ihnen die Menge der eingehenden Daten und auf den verschiedenen Tabellen können Sie weitere Informationen ablesen.

Tipp: Diese Seite macht sich an einem Fest gut, wenn man Sie auf eine Wand projiziert.