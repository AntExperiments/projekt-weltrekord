function clearProsts() {
    // clears the content of every Prost-Number when a new Proster has been selected
    document.querySelectorAll('.clearOnChange').forEach(function(element) {
        element.value = "";
    });
}

const urlParams = new URLSearchParams(window.location.search);
if (urlParams.get('success')) {
    // show success toast
    M.toast({html: 'Eintrag erfolgreich hinzugefügt!'});
    // rewrite url (without reloading) in oder to not display toast every time (this also rewrites the browser-history)
    window.history.pushState("index.php", "Projekt Weltrekord", window.location.href.split("?")[0]);
}

if (urlParams.get('error')) {
    // show success toast
    M.toast({html: 'Ein Fehler ist aufgetreten, bitte überprüffen Sie die Eingaben\noder kontaktieren Sie Ihren System-Administrator!'});
    // rewrite url (without reloading) in oder to not display toast every time (this also rewrites the browser-history)
    window.history.pushState("index.php", "Projekt Weltrekord", window.location.href.split("?")[0]);
}