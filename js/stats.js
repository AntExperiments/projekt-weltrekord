Chart.defaults.global.animation.duration = 0;

function setRegistrationStats() {
    var d = new Date();
    var ctx = document.getElementById('myChart').getContext('2d');
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: [d.getHours() - 3 + ((d.getMinutes() < 30) ? ":00" : ":30"), d.getHours() - 2 + ((d.getMinutes() < 30) ? ":30" : ":00"), d.getHours() - 2 + ((d.getMinutes() < 30) ? ":00" : ":30"), d.getHours() - 1 + ((d.getMinutes() < 30) ? ":30" : ":00"), d.getHours() - 1  + ((d.getMinutes() < 30) ? ":00" : ":30"), d.getHours() + ((d.getMinutes() < 30) ? ":30" : ":00"), d.getHours() + ((d.getMinutes() < 30) ? ":00" : ":30")],
            datasets: [{
                label: 'Anzahl Neuregistrierungen',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: registrationData.split(",")
            }]
        },
        // Configuration options go here
        options: {}
    });
}

function setProstStats() {
    var d = new Date();
    var ctx = document.getElementById('myChart2').getContext('2d');
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: [d.getHours() - 3 + ((d.getMinutes() < 30) ? ":00" : ":30"), d.getHours() - 2 + ((d.getMinutes() < 30) ? ":30" : ":00"), d.getHours() - 2 + ((d.getMinutes() < 30) ? ":00" : ":30"), d.getHours() - 1 + ((d.getMinutes() < 30) ? ":30" : ":00"), d.getHours() - 1  + ((d.getMinutes() < 30) ? ":00" : ":30"), d.getHours() + ((d.getMinutes() < 30) ? ":30" : ":00"), d.getHours() + ((d.getMinutes() < 30) ? ":00" : ":30")],
            datasets: [{
                label: 'Anzahl Prosts',
                backgroundColor: '#86baef',
                borderColor: '#86baef',
                data: registrationProstsData.split(",")
            }]
        },
        // Configuration options go here
        options: {}
    });
}

function setLeaderboard() {
    var content = leaderboardData.split("§");
    var output = "";
    for (let i = 0; i < content.length - 1; i++) {
        output += "<tr><td>" + content[i] + "</td></tr>";
    }
    document.getElementById("leaderboard").innerHTML = output;
}

function setLatestProsts() {
    var content = prostsData.split("§");
    var output = "<tr><th>Proster</th><th>Zu Proster</th></tr>";
    for (let i = 0; i < content.length - 1; i += 2) {
        output += "<tr><td>" + content[i] + "</td><td>" + content[i + 1] + "</td></tr>";
    }
    document.getElementById("latestProsts").innerHTML = output;
}

setInterval(function() {
    tick();
}, 750);
tick();

// define variables that hold the data, that's being displayed
var registrationData = "";
var registrationProstsData = "";
var leaderboardData = "";
var prostsData = "";

function tick() {
    // update variables
    updateGraph();
    updatePostGraph();
    updateLeaderboard();
    updateLatestProsts();

    // update data directly
    updateIndividualProsts();
    updateAmountOfProsts();

    // display data of graphs
    setRegistrationStats();
    setProstStats();

    // display data in tables
    setLeaderboard();
    setLatestProsts();
}

function updateGraph() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            registrationData = xhttp.responseText;
        }
    };
    xhttp.open("GET", "php/get-registries.php", true);
    xhttp.send();
}

function updatePostGraph() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            registrationProstsData = xhttp.responseText;
        }
    };
    xhttp.open("GET", "php/get-prosts.php", true);
    xhttp.send();
}

function updateLeaderboard() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            leaderboardData = xhttp.responseText;
        }
    };
    xhttp.open("GET", "php/get-prosts-leader.php", true);
    xhttp.send();
}

function updateLatestProsts() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            prostsData = xhttp.responseText;
        }
    };
    xhttp.open("GET", "php/get-prosts-latest.php", true);
    xhttp.send();
}

function updateIndividualProsts() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("individialProsts").innerText = xhttp.responseText;
        }
    };
    xhttp.open("GET", "php/get-prosts-amount.php", true);
    xhttp.send();
}

function updateAmountOfProsts() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("allProsts").innerText = xhttp.responseText;
        }
    };
    xhttp.open("GET", "php/get-prosts-amount-all.php", true);
    xhttp.send();
}