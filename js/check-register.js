function checkRegister() {
    if (document.getElementById("first_name").value.length < 3 || document.getElementById("first_name").value.length > 50) {
        M.toast({html: 'Vorname muss zwischen 3 und 50 Zeichen lang sein!'});
        return false;
    }

    if (document.getElementById("last_name").value.length < 3 || document.getElementById("last_name").value.length > 50) {
        M.toast({html: 'Nachname muss zwischen 3 und 50 Zeichen lang sein!'});
        return false;
    }

    return true;
}