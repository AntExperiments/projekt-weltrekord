<!DOCTYPE html>
<html>
<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <title>Projekt Weltrekord - Registrieren</title>

    <script src="js/utility.js" defer></script>
    <script src="js/chart.js"></script>
    <script src="js/stats.js" defer></script>

    <?php include 'php/layout.php' ?>
</head>

<body>
    <!-- Navigation Bar -->
    <?php getNav("stats") ?>

    <div class="container">
        <?php include_once("php/check-login.php"); ?>

        <!-- <h1>Live-Statistics</h1> -->
        <div class="row">
            <div class="col s12 m6">
                <h5>Anzahl Neuregistrierungen</h5>
                <canvas id="myChart"></canvas>
            </div>
            <div class="col s12 m6">
                <h5>Anzahl Prosts</h5>
                <canvas id="myChart2"></canvas>
            </div>
        </div>
        <div class="row">
            <div class="col s12 m6">
                <div class="row">
                    <h5>Allgemeine Statistiken</h5>
                    <table>
                        <tr><th>Anzahl individueller Prosts:</th><td id="individialProsts">Loading...</td></tr>
                        <tr><th>Anzahl Prosts:</th><td id="allProsts">Loading...</td></tr>
                    </table>
                </div>
                <div class="row">
                    <h5>Leaderboard</h5>
                    <table id="leaderboard">
                        <tr><td>1. Loading...</td></tr>
                        <tr><td>2. Loading...</td></tr>
                        <tr><td>3. Loading...</td></tr>
                        <tr><td>4. Loading...</td></tr>
                        <tr><td>5. Loading...</td></tr>
                    </table>
                </div>
            </div>
            <div class="col s12 m6">
                <h5>Neueste Prosts</h5>
                <table class="striped" id="latestProsts">
                    <tr><td>Loading...</td></tr>
                    <tr><td>Loading...</td></tr>
                    <tr><td>Loading...</td></tr>
                    <tr><td>Loading...</td></tr>
                </table>
            </div>
        </div>
    </div>
    
    <!-- Logout Modal -->
    <?php getModal() ?>

    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>
        